import ftplib
import logging
import os
import socket
import time


class Synchronizer(object):  # main class for FTP sync
    logger = logging.getLogger("main")
    ftp = None
    cached_stamps = []
    syncignore = False
    syncignore_path = False
    ignored_entries = []
    ignored_files = []

    def __init__(self, folder_path, ftp_uri, ftp_username, ftp_password, period):
        self.folder_path = folder_path
        self.ftp_uri = ftp_uri
        self.ftp_username = ftp_username
        self.ftp_password = ftp_password
        self.period = period

    def start(self):
        if not self.check_folder(self.folder_path):
            self.stop()

        self.syncignore = self.check_ignore()
        self.init_ignored_entries()

        try:
            self.start_ftp()
        except:
            self.stop()
            return

        self.logger.info("Started synchronizing " + self.folder_path + " to " + self.ftp_uri)

        before_files = [os.path.join(root, name) for root, dirs, files in os.walk(self.folder_path) for name in files]
        before_files = [f for f in before_files if f not in self.ignored_files]  # .syncignore check

        before_dirs = [os.path.join(root, name) for root, dirs, files in os.walk(self.folder_path) for name in dirs]

        parent_dir = self.ftp.pwd()

        self.init_cached_stamps(before_files)

        while True:  # main sync loop
            after_files, after_dirs = self.loop(parent_dir, before_files, before_dirs)
            before_files = after_files
            before_dirs = after_dirs

            if not self.ftp:
                self.start_ftp()

        self.stop()

    def start_ftp(self):  # start FTP connection
        try:
            self.ftp = ftplib.FTP(self.ftp_uri)
            self.ftp.login(self.ftp_username, self.ftp_password)
        except socket.gaierror:
            self.logger.error("Can't connect to FTP server: getaddrinfo failed")
            raise socket.gaierror
        except ftplib.error_perm:
            self.logger.error("Can't connect to FTP server: login or password incorrect")
            raise ftplib.error_perm
        except Exception as e:
            self.logger.error("Error: " + str(e))
            raise e

        self.logger.info("Connected successfully to the FTP server")
        folder_name = os.path.basename(self.folder_path)
        self.check_ftp_folder(folder_name)

    def loop(self, parent_dir, before_files, before_dirs):  # main sync loop
        time.sleep(float(self.period) / 1000.0)  # time to wait in ms

        # files
        after_files = [os.path.join(root, name) for root, dirs, files in os.walk(self.folder_path) for name in files]
        after_files = [f for f in after_files if f not in self.ignored_files]  # .syncignore check
        added_files = [f for f in after_files if f not in before_files]
        removed_files = [f for f in before_files if f not in after_files]

        # directories
        after_dirs = [os.path.join(root, name) for root, dirs, files in os.walk(self.folder_path) for name in dirs]
        added_dirs = [d for d in after_dirs if d not in before_dirs]
        removed_dirs = [d for d in before_dirs if d not in after_dirs]

        # if a file is on the FTP server, but has been added to the .syncignore file, it will be removed from the server

        # TODO : ignore directories and files in it

        self.make_ftp_directories(parent_dir, added_dirs)
        self.upload_ftp_files(parent_dir, added_files)
        self.remove_ftp_files(parent_dir, removed_files)
        self.remove_ftp_directories(parent_dir, removed_dirs)

        updated_files = self.check_updated_files(after_files)
        if len(updated_files) != 0:
            self.update_ftp_files(parent_dir, updated_files)

        return after_files, after_dirs

    def stop(self):  # close FTP
        if self.ftp is not None:
            self.ftp.close()

        self.logger.info("Exiting now.")

    def make_ftp_directories(self, parent_dir, added_dirs):  # make directories
        for d in added_dirs:
            i = d.find(parent_dir[1:])
            d = "/" + d[i:].replace("\\", "/")
            self.ftp.mkd(d)
            self.logger.info("Created directory " + d)

    def upload_ftp_files(self, parent_dir, added_files, log=True):  # upload files
        for f in added_files:
            self.cached_stamps.append({"file": f, "stamp": os.stat(f).st_mtime})
            i = f.find(parent_dir[1:])
            f_ftp_path = "/" + f[i:].replace("\\", "/")
            file = open(f, 'rb')
            self.ftp.storbinary('STOR ' + f_ftp_path, file)
            file.close()
            if log:
                self.logger.info("Uploaded file " + f)

    def remove_ftp_files(self, parent_dir, removed_files, log=True):  # remove files
        for f in removed_files:
            file_entry = (item for item in self.cached_stamps if item["file"] == f).__next__()
            self.cached_stamps.remove(file_entry)
            i = f.find(parent_dir[1:])
            f = "/" + f[i:].replace("\\", "/")
            self.ftp.delete(f)
            if log:
                self.logger.info("Removed file " + f)

    def remove_ftp_directories(self, parent_dir, removed_dirs):  # remove directories
        if removed_dirs:
            removed_dirs.reverse()  # removing from bottom to top, to avoid not empty folder exception

        for d in removed_dirs:
            i = d.find(parent_dir[1:])
            d_ftp_path = "/" + d[i:].replace("\\", "/")
            self.ftp.rmd(d_ftp_path)
            self.logger.info("Removed directory " + d)

    def check_updated_files(self, after_files):
        updated_files = []
        for f in after_files:
            file_entry = (item for item in self.cached_stamps if item["file"] == f).__next__()
            if os.stat(f).st_mtime != file_entry["stamp"]:
                updated_files.append(f)

        return updated_files

    def update_ftp_files(self, parent_dir, updated_files):
        self.remove_ftp_files(parent_dir, updated_files, log=False)
        self.upload_ftp_files(parent_dir, updated_files, log=False)
        if len(updated_files) == 1:
            self.logger.info("Updated file " + updated_files[0])
        else:
            self.logger.info("Updated files " + str(updated_files))

    def init_cached_stamps(self, files):  # init last content modification timestamps
        for file in files:
            self.cached_stamps.append({"file": file, "stamp": os.stat(file).st_mtime})

    def init_ignored_entries(self):
        ignored_entries_paths = []
        for e in self.ignored_entries:
            ignored_entries_paths.append(os.path.join(self.folder_path, e))

        self.ignored_entries = ignored_entries_paths
        self.ignored_files = [f for f in self.ignored_entries if os.path.isfile(f)]  # check if file

    def check_ignore(self):  # check if .syncignore file exists
        files = os.listdir(self.folder_path)
        if ".syncignore" in files:
            self.syncignore_path = os.path.join(self.folder_path, ".syncignore")
            self.logger.info("Found .syncignore file")
            with open(self.syncignore_path) as f:
                lines = f.readlines()
                self.ignored_entries = [line.rstrip('\n') for line in lines]

            return True
        else:
            self.logger.info("Didn't find .syncignore: no file will be ignored")

        return False

    def check_folder(self, path):  # check if folder exists
        if os.path.isdir(path):
            self.logger.info("Found folder")
            return True
        elif os.path.exists(path):
            self.logger.error("Can't find folder " + path + ": not a folder")
        else:
            self.logger.error("Can't find folder " + path + ": doesn't exist")

        return False

    def check_ftp_folder(self, folder_name):  # check if folder exists on the FTP server
        if folder_name in self.ftp.nlst():
            self.ftp.cwd(folder_name)
        else:
            self.ftp.mkd(folder_name)
            self.logger.info("Created directory " + folder_name)
            self.ftp.cwd(folder_name)
