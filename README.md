Synchronize a local folder to a remote FTP server (`rsync`-like).
You can ignore files by putting a `.syncingnore` (`.gitgnore`-like) file in the folder.
End folders with a path separator, rest are considered as files.

## Parameters :

- FTP server URI
- FTP username
- FTP password (default empty)
- Local folder
- Period (default 1000 ms)

## Documentation :

- Authors
- Dependencies
- Technical choices
