import argparse


class ArgParser(object):  # class used to parse arguments
    args = None
    parser = argparse.ArgumentParser()

    def __init__(self):
        self.parser.add_argument("--ftp_uri", help="The URI of your FTP server", required=True)
        self.parser.add_argument("--ftp_username", help="Username you want to connect with", required=True)
        self.parser.add_argument("--ftp_password", help="Password you want to connect with", default="")
        self.parser.add_argument("-f", "--folder_path", help="The path to the folder", required=True)
        self.parser.add_argument("-p", "--period", help="The refreshing period", default=1000, type=int)

    def parse_arguments(self):
        self.args = self.parser.parse_args()

    def get_argument(self, argument):  # returns the asked argument
        return {
            "ftp_uri": self.args.ftp_uri,
            "ftp_username": self.args.ftp_username,
            "ftp_password": self.args.ftp_password,
            "folder_path": self.args.folder_path,
            "period": self.args.period
        }.get(argument)
