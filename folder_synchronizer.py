import logging.config

from synchronizer import Synchronizer
from arg_parser import ArgParser

if __name__ == '__main__':
    arg_parser = ArgParser()
    arg_parser.parse_arguments()

    logging.config.fileConfig("logger.conf")
    main_logger = logging.getLogger("main")

    sync = Synchronizer(arg_parser.args.folder_path, arg_parser.args.ftp_uri, arg_parser.args.ftp_username,
                        arg_parser.args.ftp_password, arg_parser.args.period)

    sync.start()  # start the synchronization
